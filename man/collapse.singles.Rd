\name{collapse.singles}
\alias{collapse.singles}
\title{Collapse Single Nodes}
\usage{
collapse.singles(tree)
}
\arguments{
  \item{tree}{an object of class \code{"phylo"}.}
}
\description{
  This function deletes the single nodes (i.e., with a single
  descendant) in a tree.
}
\value{
  an object of class \code{"phylo"}.
}
\author{Ben Bolker}
\seealso{
  \code{\link{plot.phylo}}, \code{\link{read.tree}}
}
\keyword{manip}
